import java.util.*;

public class DoctorFactory {

    private static int totalDoctors = 0;
    private static List<Integer> freeIds = new ArrayList<>();

    private static List<String> specialities = Arrays.asList(
            "Psihologs", "Neirologs", "Acuārsts", "Pediatrs",
            "Zobārsts", "Ķirurgs", "Kardiologs", "Narkologs"
    );

//    public DoctorFactory(List<Doctor> doctors) {
//        updateFreeIds(doctors);
//    }

    // Factories
//    public Doctor create(String firstName, String lastName,
//                         String speciality, String dutyTimes){
//        totalDoctors++;
//        return new Doctor(firstName, lastName, speciality, dutyTimes);
//    }

    public Doctor create(int id, String firstName, String lastName,
                         String speciality, String dutyTimes){
        //totalDoctors++;
        return new Doctor(id, firstName, lastName, speciality, dutyTimes);
    }

    // Utilities
    public int getId(){
        if (freeIds.isEmpty()) {
            totalDoctors++;
            return totalDoctors - 1;
        } else {
            return freeIds.remove(0);
        }
    }

    public void addFreeId(Integer id){
        freeIds.add(id);
    }

    public void updateFreeIds(List<Doctor> doctors){
        if (!doctors.isEmpty()){
            doctors.sort(Comparator.comparing(Doctor::getId));
            totalDoctors = doctors.get(doctors.size() - 1).getId() + 1;
            int lastId = 0;
            for (Doctor doc:doctors) {
                if (lastId + 1 == doc.getId()){
                    lastId++;
                } else {
                    int difference = doc.getId() - lastId - 1;
                    int temp = lastId + 1;
                    lastId = doc.getId();
                    while(difference > 0){
                        if(!freeIds.contains(temp)){
                            freeIds.add(temp);
                            difference--;
                            temp++;
                        }
                    }
                }
            }
        }
    }

    public static boolean containsSpeciality(String speciality){
        return specialities.contains(speciality);
    }
}
