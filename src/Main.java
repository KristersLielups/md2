import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static Path FILE_PATH = Paths.get("data.txt");

    private static List<String> data = new ArrayList<>();
    private static List<Doctor> doctors = new ArrayList<>();

    private static DoctorFactory factory = new DoctorFactory();
    private static Scanner sc = new Scanner(System.in).useDelimiter("\\n");

    public static void main(String[] args) {
        readDatabase();
        printData();
        updateDatabase();
        printOptions();
        processInput();
    }

    /* Database operations */
    private static void readDatabase(){
        int id;
        String firstName, lastName, speciality, dutyTimes;
        try {
            data = Files.readAllLines(FILE_PATH);
            for (String entry:data) {
                String[] parsedEntry = entry.split("\\s+", 6);

                id = Integer.parseInt(parsedEntry[0]);
                firstName = parsedEntry[1];
                lastName = parsedEntry[2];
                speciality = parsedEntry[4];
                dutyTimes = parsedEntry[5];

                System.out.println("ID=" + id + "  Name=" + firstName + "  Surname=" + lastName + "  Speciality=" + speciality + "  DutyTimes=" + dutyTimes);
                Doctor newDoctor = factory.create(id, firstName, lastName, speciality, dutyTimes);
                doctors.add(newDoctor);

            }
            factory.updateFreeIds(doctors);
        } catch (IOException e){
            System.out.println("Nevar Lasit no faila");
        }
    }
    private static void updateDatabase(){
        data = new ArrayList<>();
        for (Doctor entry:doctors) {
            data.add(entry.toString());
        }
        try {
            Files.write(FILE_PATH, data, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e){
            System.out.println("Nevar Rakstit faila");
        }
    }

    /* Entry operations */
    // Add entry
    private static void addEntryUI(){
        String firstName = null, lastName = null, speciality = null, dutyTimes = null;
        System.out.print("Vārds: ");
        if (sc.hasNext())
            firstName = Validator.names(sc.next());
        if (firstName == null){
            System.out.println("Nederīga ievade!");
            addEntryUI();
            return;
        }

        System.out.print("\nUzvārds: ");
        if (sc.hasNext())
            lastName = Validator.names(sc.next());
        if (lastName == null){
            System.out.println("Nederīga ievade!");
            addEntryUI();
            return;
        }

        System.out.print("\nSpecialitāte: ");
        if (sc.hasNext())
            speciality = Validator.speciality(sc.next());
        if (speciality == null){
            System.out.println("Nederīga ievade!");
            addEntryUI();
            return;
        }

        System.out.println("\nPeim:P-Pk 12:00-16:00, S 12:00-14:00");
        System.out.print("Pieņemšanas laiki: ");
        if (sc.hasNext())
            dutyTimes = Validator.dutyTimes(sc.next());
        if (dutyTimes == null){
            System.out.println("Nederīga ievade!");
            addEntryUI();
            return;
        }
        addEntry(firstName, lastName, speciality, dutyTimes);
    }
    private static void addEntry(String firstName, String lastName,
                                 String speciality, String dutyTimes){
        int id = factory.getId();
        Doctor newDoctor = factory.create(id, firstName, lastName, speciality, dutyTimes);
        doctors.add(newDoctor);
        updateDatabase();
    }

    // Delete entry
    private static void deleteEntryUI(){
        System.out.print("Dzēst ierakstu ar id: ");
        if (sc.hasNextInt()){
            int id = sc.nextInt();
            for (Doctor doc:doctors) {
                if (doc.getId() == id){
                    doctors.remove(doc);
                    factory.addFreeId(id);
                    updateDatabase();
                    break;
                }
            }
            System.out.println("Šāds id neeksistē");
        } else {
            System.out.println("Šāds id neeksistē");
        }
    }

    // Edit entry
    private static void editEntryUI(){
        printData();
        System.out.print("\nLabojamā ieraksta id: ");
        if (sc.hasNextInt()){
            int id = sc.nextInt();
            boolean found = false;
            for (Doctor doc:doctors) {
                if (doc.getId() == id){
                    editEntry(doc);
                    found = true;
                    break;
                }
            }
            if(!found) {
                System.out.println("Šāds id neeksistē");
            }
        } else {
            System.out.println("Šāds id neeksistē");
        }
    }
    private static void editEntry(Doctor entry){
        String firstName, lastName, speciality, dutyTimes;
        boolean done = false;
        while (!done){
            System.out.println("\n" + entry.toString());
            System.out.println("Ko labot?");
            System.out.println("Vārds : 1  Uzvārds : 2  Specialitāte : 3  Pieņemšanas laiki : 4  Saglabāt : 5");
            if (sc.hasNextInt()){
                int choice = sc.nextInt();
                if (choice > 0 && choice < 6){
                    switch (choice){
                        case 1:
                            System.out.print("Vārds: ");
                            if (sc.hasNext()){
                                firstName = Validator.names(sc.next());
                                if (firstName != null) {
                                    entry.setFirstName(firstName);
                                } else {
                                    System.out.println("Nederīga ievade");
                                }
                            }
                            break;
                        case 2:
                            System.out.print("Uzvārds: ");
                            if (sc.hasNext()){
                                lastName = Validator.names(sc.next());
                                if (lastName != null) {
                                    entry.setLastName(lastName);
                                } else {
                                    System.out.println("Nederīga ievade");
                                }
                            }
                            break;
                        case 3:
                            System.out.print("Specialitāte: ");
                            if (sc.hasNext()){
                                speciality = Validator.speciality(sc.next());
                                if (speciality != null) {
                                    entry.setSpeciality(speciality);
                                } else {
                                    System.out.println("Nederīga ievade");
                                }
                            }
                            break;
                        case 4:
                            System.out.println("\nPeim:P-Pk 12:00-16:00, S 12:00-14:00");
                            System.out.print("Pieņemšanas laiki: ");
                            if (sc.hasNext()){
                                dutyTimes = Validator.dutyTimes(sc.next());
                                if (dutyTimes != null) {
                                    entry.setDutyTimes(dutyTimes);
                                } else {
                                    System.out.println("Nederīga ievade");
                                }
                            }
                            break;
                        case 5: done = true;
                            updateDatabase();
                            //sc.next();
                            break;
                    }
                } else {
                    System.out.println("Nederīga izvēle");
                }
            } else {
                System.out.println("Nederīga izvēle");
            }
        }
    }

    // Speciality Duty times
    private static void findDutyTimesUI(){///  Could Be Improved but gets the job done
        String speciality;
        boolean oneFound = false;
        System.out.print("Specialitāte: ");
        if (sc.hasNext()){
            speciality = Validator.speciality(sc.next());
            if (speciality != null) {
                System.out.println("Visi pieņemšanas laiki");
                for (Doctor doc:doctors) {
                    if (doc.getSpeciality().equals(speciality)) {
                        System.out.println(doc.getDutyTimes());
                        oneFound = true;
                    }
                }
                if (!oneFound){
                    System.out.println("Šāds ārsts nav pieejams!");
                }
                System.out.println("\n");
            } else {
                System.out.println("Šādas specialitātes nav!");
                findDutyTimesUI();
            }
        }
    }

    // Doctor numbers
    private static void getNumberOfDoctorsUI(){
        String speciality;
        System.out.print("Specialitāte: ");
        if (sc.hasNext()){
            speciality = Validator.speciality(sc.next());
            if (speciality != null) {
                int counter = 0;
                for (Doctor doc:doctors) {
                    if (doc.getSpeciality().equals(speciality))
                        counter++;
                }
                System.out.println("Sarakstā ir " + counter + " ārsti ar specialitāti " + speciality);
            } else {
                System.out.println("Nederīga ievade");
                getNumberOfDoctorsUI();
            }
        }
    }

    /* Input processing */
    private static int getUserChoice(){         //// Check else return statement.
        if (sc.hasNextInt()){                   //// Might need an additional boolean
            return sc.nextInt();
        } else {
            System.out.println("Lūdzu ievadiet skaitli");
            sc.next();
            return getUserChoice();
        }
    }
    private static void processInput(){
        int choice = getUserChoice();
        switch (choice){
            case 1: printData();
                break;
            case 2: addEntryUI();
                break;
            case 3: deleteEntryUI();
                break;
            case 4: editEntryUI();
                break;
            case 5: doctors.sort(Comparator.comparing(Doctor::getLastName));
                updateDatabase();
                break;
            case 6: doctors.sort(Comparator.comparing(Doctor::getSpeciality));
                updateDatabase();
                break;
            case 7: doctors.sort(Comparator.comparing(Doctor::getId));
                updateDatabase();
                break;
            case 8: findDutyTimesUI();
                break;
            case 9: getNumberOfDoctorsUI();
                break;
            case 0: updateDatabase();
                System.exit(1);
                break;
            default:
                System.out.println("Šādas izvēles nav!");
                processInput();
                break;
        }
    }

    /* Data output */
    private static void printData(){
        for (String line : data) {
            System.out.println(line);
        }
    }
    private static void printOptions(){
        while (0 < 1) {
            System.out.println("\n===========================================");
            System.out.println(":Izvadīt Sarakstu                       : 1");
            System.out.println(":Pievienot Jaunu Ārstu                  : 2");
            System.out.println(":Izdzēst Ārstu                          : 3");
            System.out.println(":Labot ierakstu                         : 4");
            System.out.println(":Sakārtot pēc Uzvārdiem                 : 5");
            System.out.println(":Sakārtot pēc Specialitātes             : 6");
            System.out.println(":Sakārtot pēc ID                        : 7");
            System.out.println(":Atrast Specialitātes Ārstu Laikus      : 8");
            System.out.println(":Apreiķināt Specialitātes Ārstu Skaitu  : 9");
            System.out.println(":Saglabāt un aizvērt                    : 0");
            System.out.println("===========================================\n");
            processInput();
        }
    }

    /* Helpers */
}