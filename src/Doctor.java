public class Doctor {

    private int id;
    private String firstName, lastName, speciality, dutyTimes;

    //    public Doctor(String firstName, String lastName, String speciality, String dutyTimes){
//        this.id = DoctorFactory.getId();
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.speciality = speciality;
//        this.dutyTimes = dutyTimes;
//    }
    public Doctor(int id, String firstName, String lastName, String speciality, String dutyTimes){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.speciality = speciality;
        this.dutyTimes = dutyTimes;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public String getDutyTimes() {
        return dutyTimes;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public void setDutyTimes(String dutyTimes) {
        this.dutyTimes = dutyTimes;
    }

    @Override
    public String toString() {
        return id + "\t" + firstName + "\t" + lastName + " | \t" + speciality + "\t" + dutyTimes;
    }
}
