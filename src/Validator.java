import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Validator {

    private static List<String> validDays = Arrays.asList("P", "O", "T", "C", "Pk", "S", "Sv");

    public static String speciality(String speciality){
        speciality = capitalizeFirst(speciality);
        if (DoctorFactory.containsSpeciality(speciality)){
            return speciality;
        } else {
            return null;
        }
    }

    public static String names(String firstName){
        firstName = firstName.replaceAll("\\s+","");
        firstName = capitalizeFirst(firstName);
        //if (Pattern.matches("[a-zA-Z]+", firstName)){
        if (firstName.chars().allMatch(Character::isLetter)){
            return firstName;
        } else {
            return null;
        }
    }

    public static String dutyTimes(String dutyTimes){           // P-Pk 13:00-17:00
        try {
            String[] parsedString = dutyTimes.split(", ");
            for (String shift:parsedString) {
                String[] separated = shift.split("\\s+");
                String[] days = separated[0].split("-");
                String[] times = separated[1].split("-");
                List<Integer> hours = new ArrayList<>();

                if (times.length < 2)
                    return null;
                for (String day:days) {
                    if (!isValidDay(day)) {
                        return null;
                    } else {
                        for (String time:times) {
                            hours.add(Integer.parseInt(time.split(":")[0]));
                            hours.add(Integer.parseInt(time.split(":")[1]));
                        }
                        for (Integer hour:hours) {
                            if (hour > 24 || hour < 0)
                                return null;
                        }
                    }
                }
            }
        } catch (IndexOutOfBoundsException e){
            return null;
        }

        return dutyTimes;
    }


    /* Helpers */
    private static String capitalizeFirst(String string){
        string = string.toLowerCase();
        if (string.length() > 1) {
            return string.substring(0, 1).toUpperCase() + string.substring(1);
        } else {
            return string.toUpperCase();
        }
    }
    private static boolean isValidDay(String day){
        return validDays.contains(day);
    }
}
