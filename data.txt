1	Jānis	Bērziņš | 	Psihologs	P-O 12:00-16:00, C-P 12:00-14:00
2	Artūrs	Mazuts | 	Acuārsts	C-P 10:00-17:00
3	Kaspars	Krumins | 	Neirologs	P-Pk 9:00-14:00
4	Edgars	Kalniņš | 	Acuārsts	O-C 14:00-18:00, P 12:00-16:00
5	Kaspars	Liepiņš | 	Narkologs	P-C 13:00-17:00
6	Rainis	Kārkliņš | 	Pediatrs	T-Pk 10:00-16:00
7	Vaira	Lapsa | 	Psihologs	T-Pk 9:00-15:00
8	Valdis	Ziemelis | 	Kardiologs	P-T 14:00-17:00, Pk 12:00-15:00
9	Sintija	Jansone | 	Pediatrs	P-C 12:00-16:00
10	Artūrs	Pauls | 	Acuārsts	O-T 10:00-17:00, C 12:00-17:00
